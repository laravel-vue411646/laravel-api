<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    public function register(Request $request)
    {
        // Validate all fields
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'password_confirmation' => 'required|same:password'
        ]);

        // If the validation fail, it will return all available error message as we as status code 422 and success equal to false 
        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors(),
                'status' => 422
            ];
            return response()->json($response, 422);
        }

        // If the required fields satify the validation, this will insert new user date to our database and return a success response
        $user = User::create($request->all());
        $response = [
            'success' => true,
            'user' => $user,
            'message' => [
                'passed' => ['User creation success']
            ],
            'status' => 201
        ];
        return response()->json($response, 201);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // If the validation fail, it will return all available error message as we as status code 401 and success equal to false 
        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->errors(),
                'status' => 401
            ];
            return response()->json($response, 401);
        }
        // If the required fields did not match, this will return message failure along with status code 401
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            $response = [
                'success' => false,
                'message' => [
                    'failed' => ['The credentials you entered are incorrect']
                ],
                'status' => 401
            ];
            return response()->json($response, 401);
        }

        // If the creadetials provided satisfies the condition, this will return success response with token and status code 200
        $user = Auth::user();
        $response = [
            'success' => true,
            'user' => $user,
            'message' => [
                'passed' => ['User login success']
            ],
            'token' => $user->createToken('MyApp')->plainTextToken,
            'status' => 200
        ];
        return response()->json($response, 200);
    }

    public function logout(Request $request)
    {
        // If the user passed a valid token, this will delete all available tokens in the database and return a success response
        $request->user()->tokens()->delete();
        $response = [
            'success' => true,
            'message' => [
                'passed' => ['User logout success']
            ],
            'status' => 200
        ];

        return response()->json($response, 200);
    }
}
